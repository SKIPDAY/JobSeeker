<?php
  require_once('models/job.php');
  require_once('controllers/users_controller.php');
  class JobsController {
    public function index($param) {
      $query= Job::all();
      $result = array('result' => 'success', 'data'=>$query);
      echo json_encode($result);
    }

    public function get($param) {
      $data = array();
      parse_str($param, $data);
      $query= Job::get($data['id']);
      if($query==false)
        $result = array('result' => 'failed', 'message'=>'Data tidak ditemukan');
      else
        $result = array('result' => 'success', 'data'=>$query);
      echo json_encode($result);
    }
    public function create($param) {
      $data = array();
      parse_str($param, $data);
      if(!UsersController::isLogin()){
        $result = array('result' => 'failed', 'message'=>'Masuk untuk posting job');
        echo json_encode($result);
        return;
      }
      if(Job::create($data))
        $result = array('result' => 'success', 'data'=>$data);
      else
        $result = array('result' => 'failed', 'message'=>'Gagal memasukkan data');
      echo json_encode($result);
    }
    public function update($param) {
      $data = array();
      parse_str($param, $data);
      if(!UsersController::isLogin()){
        $result = array('result' => 'failed', 'message'=>'Masuk untuk ubah job');
        echo json_encode($result);
        return;
      }
      if(Job::update($data))
        $result = array('result' => 'success', 'data'=>$data);
      else
        $result = array('result' => 'failed', 'message'=>'Gagal ubah data');
      echo json_encode($result);
    }
    public function delete($param) {
      $data = array();
      parse_str($param, $data);
      if(!UsersController::isLogin()){
        $result = array('result' => 'failed', 'message'=>'Masuk untuk hapus job');
        echo $result;
        return;
      }
      if(Job::delete($data))
        $result = array('result' => 'success', 'data'=>$data);
      else
        $result = array('result' => 'failed', 'message'=>'Gagal menghapus item');
      echo json_encode($result);
    }
    public function search($param) {
      $data = array();
      parse_str($param, $data);
      $query= Job::search($data);
      if($query==false)
        $result = array('result' => 'failed', 'message'=>'Hasil pencarian kosong');
      else
        $result = array('result' => 'success', 'data'=>$query);
      echo json_encode($result);
    }
  }
?>