angular.module('jobber', ['ngRoute', 'angularMoment']).config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'parts/landing-page.html',
        controller: 'MainController'
    }).when('/search', {
        templateUrl: 'parts/main-page.html',
        controller: 'MainController'
    }).when('/login', {
        templateUrl: 'parts/login-page.html',
        controller: 'MainController'
    }).when('/job/new', {
        templateUrl: 'parts/job-new.html',
        controller: 'MainController'
    }).when('/job/detail', {
        templateUrl: 'parts/job-detail.html',
        controller: 'MainController'
    }).otherwise({
        templateUrl: 'views/parts/404.html'
    })
}).controller('MainController', function ($scope, $location, $http) {
    //inisialisasi
    NProgress.configure({
        showSpinner: false
    });
    $scope.search = {
        'location': '',
        'keyword': ''
    };
    $scope.results = $location.search().data;
    $scope.color = [];
    if ($location.search().query === undefined)
        $scope.search = {location: "", keyword: ""}
    else if ($location.search().query.keyword === undefined)
        $scope.search = {location: "", keyword: ""}
    else
        $scope.search = $location.search().query;
    $scope.isLogged = false;
    $scope.checkForLogged = function () {
        console.log("Logged ");
        var settings = {
            async: false,
            crossDomain: true,
            url: "route.php/users/is-login",
            method: "GET",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        };
        $.ajax(settings).done(function (response) {
            $scope.isLogged = response == "false" ? Boolean(false) : Boolean(true);
            //new Boolean(true);
            console.log(response == "false");
        });
    };
    $scope.cari2 = function (query) {
        NProgress.start();
        var settings = {
            async: true,
            crossDomain: true,
            url: "route.php/jobs/search",
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: query
        };
        $.ajax(settings).done(function (response) {
            NProgress.done();
            var response = $.parseJSON(response);
            if (response.result === "failed") {
                toastr.error(response.message);
                return;
            }
            $scope.color = [];
            for (var i = 0; i < response.data.length; i++) {
                $scope.color.push($scope.getColor(300));
            }
            response.color = $scope.color;
            $location.search({
                data: response,
                query: query
            });
            $location.path("/search");
            $scope.results = response;
            console.log($scope.results.color);
            $scope.$apply();
            $location.replace();
        });
    };
    $scope.cari = function (query) {
        NProgress.start();
        $location.path("/search").search("");
        var settings = {
            async: true,
            crossDomain: true,
            url: "route.php/jobs/search",
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: query
        };
        $.ajax(settings).done(function (response) {
            NProgress.done();
            var response = $.parseJSON(response);
            if (response.result === "failed") {
                toastr.error(response.message);
                return;
            }
            $scope.color = [];
            for (var i = 0; i < response.data.length; i++) {
                $scope.color.push($scope.getColor(300));
            }
            response.color = $scope.color;
            $location.search({
                data: response,
                query: query
            });
            $location.path("/search");
            $scope.results = response;
            $(function () {
                $("#job-description").each(function (i) {
                    var len = $(this).text().length;
                    if (len > 10) {
                        $(this).text($(this).text().substr(0, 10) + '...');
                    }
                });
            });
            $scope.$apply();
            $location.replace();
        });
    };
    $scope.trimText = function (text, length) {
        console.log(length);
        if (text.length > length)
            return text.substr(0, length) + '...';
        else
            return text;
    };
    $scope.postingJobToggle = function () {
        console.log(typeof $scope.isLogged);
        if (!$scope.isLogged) {
            $location.path("/login").search({action: "post-job"});
            console.log("/login");
        } else {
            $location.path("/job/new").search("");
            console.log("/job/new");
        }
    };
    $scope.loginToggle = function () {
        if ($scope.isLogged) {
            $scope.logout();
            return;
        }
        $location.path("/login").search("");
    };
    $scope.signUp = function (user) {
        NProgress.start();
        var settings = {
            async: true,
            crossDomain: true,
            url: "route.php/users/create",
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: user
        };
        $.ajax(settings).done(function (response) {
            NProgress.done();
            var response = $.parseJSON(response);
            console.log(response);
            if (response.result === "success") {
                toastr.info(response.data);
            } else {
                toastr.error(response.message);
            }
        });
    };
    $scope.login = function (user) {
        NProgress.start();
        var settings = {
            async: true,
            crossDomain: true,
            url: "route.php/users/login",
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: user
        };
        $.ajax(settings).done(function (response) {
            NProgress.done();
            var response = $.parseJSON(response);
            if (response.result === "success") {
                $scope.isLogged = true;
                if ($location.search().action != undefined) {
                    switch ($location.search().action) {
                        case 'post-job':
                        {
                            $location.path("/job/new").search("");
                            break;
                        }
                        default:
                        {
                            $location.path("/search").search("");
                            break;
                        }
                    }
                } else {
                    $location.path("/search").search("");
                }
                $scope.$apply();
                $location.replace();
            } else {
                toastr.error(response.message);
            }
        });
    };
    $scope.logout = function () {
        NProgress.start();
        var settings = {
            async: true,
            crossDomain: true,
            url: "route.php/users/logout",
            method: "GET",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        };
        $.ajax(settings).done(function (response) {
            NProgress.done();
            //noinspection JSDuplicatedDeclaration
            var response = $.parseJSON(response);
            console.log(response.result);
            if (response.result === "success") {
                $scope.isLogged = false;
                $location.path("/login");
                $scope.$apply();
                $location.replace();
            } else {
                toastr.error(response.message);
            }
        });
    };
    $scope.newJob = function (job) {
        NProgress.start();
        var settings = {
            async: true,
            crossDomain: true,
            url: "route.php/jobs/create",
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: job
        };
        $.ajax(settings).done(function (response) {
            NProgress.done();
            var response = $.parseJSON(response);
            if (response.result === "success") {

                $scope.$apply();
                $location.replace();
            } else {
                toastr.error(response.message);
            }
        });
    };
    $scope.getColor = function (intensity) {
        return palette.random(intensity);
    }

});
