<?php
  class User {
    public static function exists($email) {
      $db = Db::getInstance();
      $req = $db->prepare('SELECT * FROM users WHERE email = :email');
      $req->execute(array('email' => $email));
      if($req->fetchColumn())
        return true;
      return false;
    }
    public static function create($data){
      $db = Db::getInstance();
      $req = $db->prepare("INSERT INTO users(
            name,
            email,
            password,
            created) VALUES (
            :name, 
            :email, 
            :password,
            :created)");
      $req->execute(array(
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => md5($data['password']), 
        'created' => date("Y-m-d H:i:s")
        )
      );
    }
    public static function login($data){
      $db = Db::getInstance();
      $req = $db->prepare('SELECT * FROM users WHERE email = :email AND password = :password');
      $req->execute(array(
        'email' => $data['email'],
        'password' => md5($data['password']
        )
      )
      );
      $user = $req->fetch(PDO::FETCH_ASSOC);
      if(!$user)
        return false;
      return $user;
    }
  }
?>