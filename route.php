<?php
  	require_once('connection.php');
  	require_once('controllers/jobs_controller.php');
  	require_once('controllers/users_controller.php');
  	$url = explode('/', dirname($_SERVER['PHP_SELF']));
	$controller_type = $url[count($url) - 1]; 
	$action=basename($_SERVER['PHP_SELF']);
	if ($controller_type=='users') {
		$controller = new UsersController();
		$data=file_get_contents('php://input');
		switch ($action) {
			case 'login':
				$controller->login($data);
				break;
			case 'create':
				$controller->create($data);
				break;
			case 'logout':
				$controller->logout();
				break;
			case 'is-login':
				$controller->isLogin();
			break;
			default:
				$result = array('result' => 'failed', 'message'=>'metode tidak ditemukan');
      			echo json_encode($result);
				break;
		}
	}elseif ($controller_type=='jobs') {
		$controller = new JobsController();
		$data=file_get_contents('php://input');
		switch ($action) {
			case 'index':
				$controller->index($data);
				break;
			case 'create':
				$controller->create($data);
				break;
			case 'get':
				$controller->get($data);
			break;
			case 'delete':
				$controller->delete($data);
			break;
			case 'update':
				$controller->update($data);
			break;
			case 'search':
				$controller->search($data);
			break;
			default:
				$result = array('result' => 'failed', 'message'=>'metode tidak ditemukan');
      			echo json_encode($result);
				break;
		}
	}else{
		$result = array('result' => 'failed', 'message'=>'kontroller tidak ditemukan');
      	echo json_encode($result);
	}
?>
